import java.util.Scanner;

public class ExPOO_S_22 {

	// declara��o das vari�veis globais e statics
	static Scanner tecla = new Scanner(System.in);
	
	public static void main (String[] args) {
		
		// declara��o de vari�veis locais e contantes
		int manOld=0, manYoung=0, womanOld=0, womanYoung=0;
		int valor1, valor2, valor3, valor4, soma, produto;
		
		// entrada de dados
		System.out.println("Programa para verificar alguma coisa.");
		System.out.println("                                            ");
		System.out.println("Digite a idade do primeiro homem: ");
		valor1 = tecla.nextInt();
		System.out.println("Digite a idade do segundo homem: ");
		valor2 = tecla.nextInt();
		System.out.println("Digite a idade da primeira mulher: ");
		valor3 = tecla.nextInt();
		System.out.println("Digite a idade da primeira mulher: ");
		valor4 = tecla.nextInt();
		
		// processamento de dados
		if (valor1 > valor2) {
			manOld = valor1;
			manYoung = valor2;
		}else {
			manYoung = valor1;
			manOld = valor2;
		}
		
		if (valor3 > valor4) {
			womanOld = valor3;
			womanYoung = valor4;
		}else {
			womanYoung = valor3;
			womanOld = valor4;
		}
		
		soma = Math.addExact(manOld, womanYoung);
		produto = Math.multiplyExact(manYoung, womanOld);
		
		// sa�da de dados
		System.out.println("Soma das idades do homem mais velho com a mulher mais nova �: " + soma );
		System.out.println("Produto das idades do homem mais novo com a mulher mais velha �: " + produto );
	}
}
