import java.util.Scanner;

public class ExPOO_1 {
	
	// Declara��o de vari�veis global e static
	static Scanner tecla = new Scanner (System.in);
	
	public static void main(String[] args) {
		
		// Declara��o de vari�veis locais e constantes
		
		double raio, area;
		final double PI = 3.14;
		
		// Entrada de dados
		
		System.out.println("Digite o valor do raio: ");
		raio = tecla.nextDouble();
		
		// Processamento de dados
		
		area = PI * Math.pow(raio,2);
		
		// Sa�da de informa��es
		
		System.out.println("O valor da �rea � = " + area);
		
		

	}

}
