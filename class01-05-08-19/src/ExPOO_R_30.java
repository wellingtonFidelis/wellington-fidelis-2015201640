import java.util.Scanner;

public class ExPOO_R_30 {

	// declaração de variáveis globais e statics
	static Scanner teclado = new Scanner(System.in);
	static String aluno ;
	static String dado1 ;
	static boolean dado;

	static public void main (String[] args) {
		
			//declaração de variáves locais e constantes
			double nota1, nota2, media;
			
			String resultado = null;
		
		do {	
			do {
				
				//entrada de dados and processing data
				System.out.print("Digite o nome do aluno: ");
				aluno = teclado.next();
				System.out.print("Digite o valor da 1 avaliação: ");
				nota1 = teclado.nextDouble();		
				analiseNota(nota1);
				
				System.out.print("Digite o valor da 2 avaliação: ");
				nota2 = teclado.nextDouble();		
				analiseNota(nota2);
				
				System.out.println("Os dados estão corretos (S/N)? ");
				dado1 = teclado.next();
				
				do {
					if (dado1.equals("N")) {
						dado = true; break;
						
					} else if (dado1.equals("S")) {
						dado = false; break;
						
					} else {
						System.out.println("Digite apenas S para sim ou N para não: ");
						dado1 = teclado.next();
					}
					
				} while (dado = true);
				
				
			} while (dado1.equals("N"));
			
			media = (nota1 + nota2)/2;
			
			// output data
			System.out.print("Aluno: " + aluno);
			System.out.println("\n_________________________________________________________");
			System.out.println("1ª Avaliação	 2ªAvaliação	Média	 Resultado");
			if (media < 3) {
					resultado = "Reprovado";	
					} else if (media >= 3 && media < 6) {
						resultado = "Em exame";
					} else if (media >= 6) {
						resultado = "Aprovado";
					}
			System.out.println(nota1 + "		 "+ nota2 + "		"+ media + "	" + resultado);
			System.out.println("_________________________________________________________");
			
			if (resultado.equals("Reprovado")) {
				System.out.println("\n Obs: Você deverá obter nota 6.0 no exame de aprovação!");
			}
		
			System.out.println("");
			
			System.out.println("Novo Cálculo (S/N)? ");
			dado1 = teclado.next();
			
			do {
				if (dado1.equals("N")) {
					dado = true; break;
					
				} else if (dado1.equals("S")) {
					dado = false; break;
					
				} else {
					System.out.println("Digite apenas S para sim ou N para não: ");
					dado1 = teclado.next();
				}
				
			} while (dado = true);
		}while (dado1.equals("S"));	
	}
	
//---------------------------------------------------------------------------------------
	//Methods
	
static public double analiseNota (double x) {
	do {
		if (x < 0 || x > 10) {
			System.out.println("Valor de nota incorreto!");
			System.out.println("Digite um valor maior que 0 e menor que 10: ");
			x = teclado.nextDouble();
		}
	}while (x < 0.0 || x > 10.0);
	
	return x;
}

	
}
