import java.util.Scanner;

public class ExPOO_M_4 {
	
	// declaration of variables Globals and static
	static Scanner Scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		// declaration of variables locals and constants		
		int[][] A = new int [4][4];
		int somaA=0, somaB=0, somaC=0, incremento=0;
		
		//data input
		for (int i=0; i < A.length; i++) {
			for (int j=0; j < A[i].length; j++) {
				incremento += 1;
				A[i][j] += incremento;
				//System.out.print(A[i][j] + "      ");
			}
			//System.out.println("");
		}
		
		// data processing
		// sum of line 3
		for (int i=0; i < A.length; i++) {
			for (int j=0; j < A[i].length; j++) {
				if (i == 3) {
					somaA += A[i][j];
				}
			}
		}
		
		// sum of column 2
		for (int i=0; i < A.length; i++) {
			for (int j=0; j < A[i].length; j++) {
				if (j == 2) {
					somaB += A[i][j];
				}
			}
		}
		// sum of all Matriz
		for (int i=0; i < A.length; i++) {
			for (int j=0; j < A[i].length; j++) {
				somaC += A[i][j];
			}
		}
		
		// data output
		System.out.println("A soma da linha 3 �: " + somaA);
		System.out.println("");
		System.out.println("A soma da coluna 2 �: " + somaB);
		System.out.println("");
		System.out.println("A soma de toda a matriz �: " + somaC);
	}

}
