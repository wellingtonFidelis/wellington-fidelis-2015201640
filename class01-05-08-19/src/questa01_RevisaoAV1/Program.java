package questa01_RevisaoAV1;

import java.util.Scanner;

public class Program {
	// declaration of variables globals and static
		static Scanner scan = new Scanner(System.in);
	// constant
    static final int MAXCONTA = 20;
   
    // common variable
    static int index = 0;
    
    // total of persons
    static Pessoa[] lista = new Pessoa[MAXCONTA];

	public static void main(String[] args) {
		// declaration of variables of locals and constants

		int op; // variable for menu
		
		do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Cadastrar pessoa");
            System.out.println("2-Listar");
            System.out.println("3-Sair");
            op = scan.nextInt(); 
            switch(op){
                case 1: cadastrarPessoa(); break;
                case 2: listar(); break;
                case 3: break;
            }
        } while (op!=3);    

	}
	
	//methods
	
	public static void cadastrarPessoa() {
		//input
        System.out.println("Digite o nome da pessoa: ");
        String nome = scan.next();
        System.out.println("Digite a idade: ");
        int idade = scan.nextInt();

        // generate a object of type Pessoa and add on lista
        lista[index++] = new Pessoa(nome, idade);
        System.out.println("Conta cadastrada com sucesso!");
        System.out.println("                               ");
	}
	
	public static void listar () {
		 int totalPeople = 0;
	        System.out.println("NAME: ---------- AGE:");
	        for (int i = 0; i < lista.length-1; i++) {
	            if (lista[i] != null){
	                System.out.println(lista[i].getNome()
	                                   + " ---------- " +
	                                   lista[i].getIdade());
	                
	                totalPeople =+ totalPeople + 1;
	                
	            }else{
	            	
	            }
	        }
	        System.out.println("Total de Pessoas:----------" + totalPeople);
	}

}