package questa01_RevisaoAV1;

public class Pessoa {
	
	private String nome;
	private int idade;
	private boolean status;
	
	// methods getters and setters
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

	
	// Constructor
	public Pessoa (String nome, int idade) {
		this.nome = nome;
		this.idade = idade;
		this.status = true;
	}

}
