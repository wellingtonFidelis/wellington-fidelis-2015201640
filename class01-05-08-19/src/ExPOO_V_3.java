import java.util.Scanner;

public class ExPOO_V_3 {

	// declarations of variables globals and static
	static Scanner teclado = new Scanner(System.in);
	
	 public static void main (String[] args) {
		// declarations of variables locals and constant
		int B [] = new int [10];
		
		// data input
		System.out.println("Programa para ler um vetor e imprimi-lo.");
		System.out.println("-------------------------------------------------------------------");
		System.out.println("O valor de cada �ndice ser� de 0 ou 1.");
		System.out.println("-------------------------------------------------------------------");
		
		// calculations
		for (int l=0;l<B.length;l++) {
			B[l] = analise1 (l);
			}
		
		// data out
		for (int l=0;l<B.length;l++) {
		System.out.println("O valor do �ndice " +l+ " �: " + B[l]);
		}
		
	}
//*******************************************************************************************************************	
	//methods
	static private int analise1 (int x) {
		if (Math.floorMod(x, 2) == 0) {
			x=0;
		}else {
			x=1;
			}
		
		return x;
	}
}
