import java.util.Scanner;

public class ExPOO_R_6 {

	// declaração variáveis globais e statics
	static Scanner tecla = new Scanner(System.in);
	
	static public void main (String[] args) {
		
		int resposta=1;
		
		do {
				
			//declaração de variáveis globais e constantes
			double nota1=0, nota2=0, result=0;
			
			// entrada de dados e processamento
			System.out.println("Programa para cálculo de nota semestral.");
			System.out.println("                                                     ");
			System.out.println("Digite o valor da 1 avaliação: ");
			nota1 = tecla.nextDouble();
			if (nota1 < 0 || nota1 > 10) {
				do {
					System.out.println("Nota Inválida!");
					System.out.println("Digite outro valor para 1 avaliação: ");
					nota1 = tecla.nextDouble();
				} while (nota1 < 0 || nota1 > 10);
			}
			
			System.out.println("Digite o valor da 2 avaliação: ");
			nota2 = tecla.nextDouble();
			
			if (nota2 < 0 || nota2 > 10) {
				do {
					System.out.println("Nota Inválida!");
					System.out.println("Digite outro valor para 2 avaliação: ");
					nota2 = tecla.nextDouble();
				} while (nota2 < 0 || nota2 > 10);
			}
			
			result = (nota1 + nota2) / 2;
			
			// saída de dados
			
			System.out.println("--------------------------------------------------------------");
			System.out.println("Média semestral é = " + result );
			System.out.println("--------------------------------------------------------------");
			System.out.println("Novo cálculo, 1.Sim ou 2.Não? ");
			resposta = tecla.nextInt();
		
		} while (resposta == 1);
		
	}
}
