import java.util.Scanner;

public class ExPOO_5 {

	// declaração de variáveis globais e static
	static Scanner tecla = new Scanner(System.in);
	
	public static void main (String[] args) {
		// declaração de variáveis locais e contantes
		double altura, comprimento, largura, area1, area2, area3, areaTotal, quantidadeAzulejo;
		
		// entrada de dados
		System.out.println("Programa para calculo da quantidade de cx. az para uma certa área.");
		System.out.println("                                                                  ");
		System.out.println("Digite o valor da largura do cômodo, em metros: ");
		largura = tecla.nextDouble();
		System.out.println("Digite o valor da altura do cômodo, em metros: ");
		altura = tecla.nextDouble();
		System.out.println("Digite o valor do comprimento do cômodo, em metros: ");
		comprimento = tecla.nextDouble();
		
		// processando dados
		area1 = altura * comprimento;
		area2 = altura * largura;
		area3 = largura * comprimento;
		
		areaTotal = area1 + area2 + area3;
		
		quantidadeAzulejo = areaTotal / 1.5;
		
		// saída de dados
		
		System.out.println("A quantidade de CX. de Azulejos será de " + Math.ceil(quantidadeAzulejo) + " para " + areaTotal + " m².");
	}
	
}
