package questao03_RevisaoAV1;

import java.util.Scanner;

public class Program {
	
	// declaration of variables statics and globals
	static Scanner scan = new Scanner(System.in);
	static final int maximoCandidatos = 10;
	static int index = 0;
	
	// list of candidatos
	static Candidato[] lista = new Candidato[maximoCandidatos];
	
	public static void main(String[] args) {
		// declarations of variables locals and constants
		int opcao = 0;
		
		do {
			System.out.println("Digite o n�mero da op��o: ");
			System.out.println("1 - Cadastrar novo candidato.");
			System.out.println("2 - Listar candidatos.");
			System.out.println("3 - Acrescentar votos.");
			System.out.println("4 - Calcular vencedor.");
			System.out.println("9 - Sair.");
			opcao = scan.nextInt();
			

			switch (opcao) {
				case 1: cadastrarNovoCandidato(); break;
				case 2: listarCandidatos(); break;
				case 3: acrescentarVotos(); break;
				case 4: calcularVencedor(); break;
			}
			
			
		} while (opcao != 9);
		
	}
	
	// methods
	
	// method to create new candidato
	public static void cadastrarNovoCandidato () {
		System.out.println(" ");
		System.out.println("Digite o n�mero do Canditado: ");
		String numeroCandidato = scan.next();
		System.out.println("Digite o nome do candidato: ");
		String nomeCandidato = scan.next();
		
		// criando um obejto do tipo candidato
		lista[index++] = new Candidato(numeroCandidato, nomeCandidato, 0);
		System.out.println(" ");
		System.out.println("O candidato " + nomeCandidato + " no n�mero " + numeroCandidato + 
				" foi cadastrado com sucesso!");
		System.out.println(" ");
	}
	
	// method to list all candidatos cadastrados
	public static void listarCandidatos () {
		System.out.println("N�mero do candidato __________ Nome de candidato __________ Qtd. Votos");
		
		for (int i=0; i < lista.length; i++) {
			if (lista[i] != null) {
				
				Candidato C = (Candidato)lista[i];
				if (C instanceof Candidato) {
					System.out.println(C.getNumeroCandidato() + "__________" + C.getNomeCandidato()
										+ "__________" + C.getNumeroVotos());
					System.out.println(" ");
				}
			}
		}
	}
	
	// method para incrementar o n�mero de votos de cada candidato
	public static void acrescentarVotos () {
		System.out.println("Digite o N�mero do candidato: ");
		String numeroCandidato = scan.next();
		System.out.println("Digite a quantidade dos votos: ");
		long numeroVotos = scan.nextInt();
		for (int i=0; i < lista.length; i++) {
			if (lista[i] != null) {
				if (lista[i].getNumeroCandidato().equals(numeroCandidato)) {
					lista[i].somarVotos(numeroVotos);
					System.out.println("O Candidato " + lista[i].getNomeCandidato() + ", est� com " +
							lista[i].getNumeroVotos() + " votos.");
					break;
				}
			}
		}
	}
	
	// method para calcular o vencedor
	public static void calcularVencedor () {
		
		/* criar um array com os tres primeiros colocados
		 * mostrar os nomes e a porcentagem de de votos com base no total */
		
		// criando um array para salvar numero nome porcentagem posi��o
		String[][] vencedores = new String [3][4];
		
		long totalVotos = 0;
		long maior =0;
		// for to calculate total of votes
		for (int i=0; i < lista.length; i ++) {
			if (lista[i] != null) {
				totalVotos += lista[i].getNumeroVotos(); 
				
				if (maior > lista[i].getNumeroVotos()) {
					maior = lista[i].getNumeroVotos();
				}
			}
		}
		// for to calculate percent of each candidato
		for (int i=0; i < lista.length; i ++) {
			if (lista[i] != null) {
				for (int j=0; j < vencedores[i].length; j++) {
					vencedores[i][j] = lista[i].getNumeroCandidato();
					vencedores[i][j+1] = lista[i].getNomeCandidato();
					vencedores[i][j+2] = Long.toString(lista[i].getNumeroVotos() / totalVotos);
					if (maior == lista[i].getNumeroVotos()) {
						vencedores[i][j+3] = "1";
					}
					continue;
				}
			}
		}
	}
}
