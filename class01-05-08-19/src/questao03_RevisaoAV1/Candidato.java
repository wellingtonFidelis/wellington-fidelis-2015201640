package questao03_RevisaoAV1;

public class Candidato {
	private String numeroCandidato;
	private String nomeCandidato;
	private long numeroVotos;
	
	// constructor
	public Candidato(String numeroCandidato, String nomeCandidato, long numeroVotos) {
		super();
		this.numeroCandidato = numeroCandidato;
		this.nomeCandidato = nomeCandidato;
		this.numeroVotos = numeroVotos;
	}
	
	// Getters and Setters
	public String getNumeroCandidato() {
		return numeroCandidato;
	}

	public void setNumeroCandidato(String numeroCandidato) {
		this.numeroCandidato = numeroCandidato;
	}

	public String getNomeCandidato() {
		return nomeCandidato;
	}

	public void setNomeCandidato(String nomeCandidato) {
		this.nomeCandidato = nomeCandidato;
	}

	public long getNumeroVotos() {
		return numeroVotos;
	}

	public void setNumeroVotos(long numeroVotos) {
		this.numeroVotos = numeroVotos;
	}
	
	// somar votos
	public void somarVotos(long valor){
		numeroVotos += valor;
   }
	
}
