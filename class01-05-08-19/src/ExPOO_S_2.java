import java.util.Scanner;

public class ExPOO_S_2 {

	//declaração de variáveis globais e static
	static Scanner tecla = new Scanner(System.in);
	
	public static void main (String[] args) {
		
		//delclaração de variáveis locais e consntantes
		double nota1, nota2, nota3, media=0;
		
		//entrada de dados
		System.out.println("Programa para calculo da média semetral.");
		System.out.println("                                        ");
		System.out.println("Digite o valor da nota da primeira prova: ");
		nota1 = tecla.nextDouble();
		System.out.println("                                        ");
		System.out.println("Digite o valor da nota da segunda prova: ");
		nota2 = tecla.nextDouble();
		System.out.println("                                        ");
		System.out.println("Digite o valor da nota da prova optativa, se não realizou digite '-1': ");
		nota3 = tecla.nextDouble();
		
		//processamento de dados
		if (nota3 >= 0) {
			if (nota3 > nota2) {
				media = (nota1 + nota3)/2;
			}else if (nota3 > nota1) {
				media = (nota2 + nota3)/2;
			}		
		}else {
			media = (nota1 + nota2)/2;
		}
		
		//saída de dados
		if (media >= 6) {
			System.out.println("Aprovado");
		}else if (media < 6 && media >= 3){
			System.out.println("Está em exame");
		}else {
			System.out.println("Reprovado");
		}
	}
}