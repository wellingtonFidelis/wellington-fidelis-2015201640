package aula_10_09_2019;

import java.util.Scanner;

public class Poupanca extends Conta {
	
	static Scanner tecla = new Scanner(System.in);
	
	private double taxaRemuneração = 1.03;

	public double getTaxaRemuneração() {
		return taxaRemuneração;
	}

	public void setTaxaRemuneração(double taxaRemuneração) {
		this.taxaRemuneração = taxaRemuneração;
	}

	public Poupanca(int numero, double saldo) {
		super(numero, saldo);
	}
	
	@Override
	public void depositar(double valor) {
		valor = valor * taxaRemuneração;
		setSaldo(getSaldo() + valor);
		}
	
	public void print() {
		System.out.println(getNumero() + "\t" +
			   getSaldo() + "\t"); 
	}
	}
