import java.util.Scanner;

public class ExPOO_R_7_test {


	// declaração variáveis globais e statics
		static Scanner tecla = new Scanner(System.in);
		
		static public void main (String[] args) {
			
			int resposta=1;
			
			do {
					
				//declaração de variáveis globais e constantes
				double nota1=0, nota2=0, result=0;
				
				// entrada de dados e processamento
				System.out.println("Programa para cálculo de nota semestral.");
				System.out.println("                                                     ");
				System.out.println("Digite o valor da 1 avaliação: ");
				nota1 = tecla.nextDouble();
				
				analise (nota1);
				
				System.out.println("Digite o valor da 2 avaliação: ");
				nota2 = tecla.nextDouble();
				
				analise (nota2);
				
				result = (nota1 + nota2) / 2;
				
				// saída de dados
				
				System.out.println("--------------------------------------------------------------");
				System.out.println("Média semestral é = " + result );
				System.out.println("--------------------------------------------------------------");
				System.out.println("Novo cálculo, 1.Sim ou 2.Não? ");
				resposta = tecla.nextInt();
				
				analise (resposta);
					
			} while (resposta == 1);			
		}
//------------------------------------------------------------------------------------------		
// methods
		
		static public void analise (double x) {
			if (x < 0 || x > 10) {
				do {
					System.out.println("Nota Inválida!");
					System.out.println("Digite outro valor para 1 avaliação: ");
					x = tecla.nextDouble();
				} while (x < 0 || x > 10);
			}
		}
		
		static public void analise (int x) {
			do {
				if (x != 1 && x != 2) {
					System.out.println("Valor incorreto, digite 1 para Sim ou 2 Não.");
					x =  tecla.nextInt();
				}
			} while (x != 1 && x != 2);
		}
}
