import java.util.Scanner;

public class ExPOO_4 {
	
	// declaração de varáveis globais e estatcis
	static Scanner tecla = new Scanner (System.in);
	
	public static void main(String[] args) {
		
		// declaração de variáveis local e constantes
		int potenciaLampada;
		double largura, comprimento, metroquadrado, potenciaTotal, quantidadeLampada;
		
		// entrada de dados
		System.out.println("Programa para calcular o número de lampadas necessarias para iluminar um comodo.");
		System.out.println("                                                                      ");
		System.out.println("Digite a potência da lâmpada a ser utilizada, em Watts:");
		potenciaLampada = tecla.nextInt();
		
		System.out.println("Digite o tamanho da largura do cômodo, em metros:");
		largura = tecla.nextDouble();
		System.out.println("Digite o tamanho do comprimento do cômodo, em metros:");
		comprimento = tecla.nextDouble();
		
		// processamento de dados
			//calculo do metro quadrado
			metroquadrado = largura * comprimento;
			// calculo da potencia total
			potenciaTotal = 18 * metroquadrado;
			// calculo da quantidade de lampadas necessarias
			quantidadeLampada = potenciaTotal / potenciaLampada;
			
		// saída de dados
		System.out.println("A quantidade de lampadas para iluminar " + metroquadrado + " m² é = " + quantidadeLampada + " ou " 
				+ Math.ceil(quantidadeLampada) + " lâmpadas.");
		
	}	
}
	
	
