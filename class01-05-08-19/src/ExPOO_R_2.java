import java.util.Scanner;

public class ExPOO_R_2 {
	
	// declaração de variáveis globais e statics
		static Scanner tecla = new Scanner(System.in);
		
		public static void main (String[] args) {
			
			// declaração de variáveis locais e constantes
			double result, dividendo, divisor;
			
			// entrada de dados
			System.out.println("Programa para divisão de dois números.");
			System.out.println("                                                     ");
			System.out.println("Digite o dividendo: ");
			dividendo = tecla.nextDouble();
			System.out.println("Digite o divisor: ");
			divisor = tecla.nextDouble();
			
			// processamento de dados
			do {
				if (divisor == 0) {
					System.out.println("Valor inválido!");
					System.out.println("Digite outro valor para divisor: ");
					divisor = tecla.nextDouble();
				}
			} while (divisor == 0);
			
			result =  dividendo / divisor;
			
			// saída de dados
			System.out.println("O resultada da divisão é: " + result);

}
}
