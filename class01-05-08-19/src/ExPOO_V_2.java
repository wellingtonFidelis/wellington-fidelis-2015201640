import java.util.Scanner;

public class ExPOO_V_2 {

	//declara��o de vari�veis globais e statics
	static Scanner teclado = new Scanner(System.in);
	
	public static void main (String[] args) {
		
		//declara��o de vari�veis locais e constantes
		int A [] = new int [10];
		
		// entrada de dados
		System.out.println("Programa para ler um vetor e imprimi-lo.");
		System.out.println("-------------------------------------------------------------------");
		System.out.println("O valor de cada �ndice ser� de 1 a 10.");
		System.out.println("-------------------------------------------------------------------");
		
		// processamento de dados
		for (int l = 0 ; l < A.length ; l++ ) {
			A[l] = l + 1;
		}
		
		// sa�da de dados
		for (int l = 0 ; l < A.length ; l++) {
			System.out.println("o valor do �ndice " + l + " �: " + A[l]);
		}
	}
}
