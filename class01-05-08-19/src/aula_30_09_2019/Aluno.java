package aula_30_09_2019;

public class Aluno extends Pessoa {
	// Atributos
	 private long matricula;
	 private double a1, a2;

	 // Construtor
	 public Aluno (String n, long id, long m) {
	 super(n, id); //passa par�metros para o construtor da classe-base
	 matricula = m;
	 }
	 // M�todo para receber notas
	 public void setNotas(double n1, double n2) {
	 a1 = n1; a2 = n2;
	 }
	 // M�todo para calcular e retornar a m�dia
	 public double media() {
	 return (a1+a2)/2;
	 }

	// M�todo para imprimir dados do aluno
	 public void print() {
	 super.print(); // chama print da classe-base
	 System.out.println("Matricula: " + matricula);
	 System.out.println("A1=" + a1 + " A2=" + a2 + " Media=" + media());
	 } 

}
