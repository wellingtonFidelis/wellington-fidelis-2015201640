package aula_30_09_2019;

public class TestePolimorfismoHerançaParametros {
	public static void main(String args[]) {
		 Aluno a = new Aluno("Jorge", 1234, 2009001001);
		 a.setNotas(9.0, 8.0);


		 Funcionario f = new Funcionario("Ana", "Gerente", 12345, 3000);

		 Relatorio relatorio = new Relatorio();

		 System.out.println();
		 System.out.println("Relatorio de aluno:");
		 relatorio.imprimir(a);
		 System.out.println();
		 System.out.println("Relatorio de funcionario:");
		 relatorio.imprimir(f);

		 } 
}
