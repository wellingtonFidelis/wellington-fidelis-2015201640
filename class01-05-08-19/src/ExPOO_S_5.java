import java.util.Scanner;

public class ExPOO_S_5 {
	
	// declara��o de vari�veis globais e statics
	static Scanner tecla = new Scanner(System.in);
	
	public static void main (String[] args) {
		
		// declara��o de vari�veis locais e constantes
		double valor;
		String resultado;
		
		// entrada de dados
		System.out.println("Programa para verificar se o n�mero � positivo ou n�o");
		System.out.println("                                            ");
		System.out.println("Digite o valor");
		valor = tecla.nextDouble();
		
		//processamento de dados
		if (valor < 0 ) {
			resultado = "O n�mero � negativo.";
		}else {
			resultado = "O n�mero � positivo.";
		}
		
		// sa�da de dados
		System.out.println(resultado);
	}
}
