import java.util.Scanner;

public class ExPOO_2 {

	// Declara��o de vari�veis global e static
		static Scanner tecla = new Scanner (System.in);
		
		public static void main(String[] args) {
			
			// Declara��o de vari�veis locais e constantes
			
			double celsius, fahrenheit;
			
			// Entrada de dados
			
			System.out.println("Programa para calcular a conver��o de F para C.");
			
			System.out.println("Digite o valor em Fahrenheit: ");
			fahrenheit = tecla.nextDouble();
			
			// Processamento de dados
			
			celsius = ((fahrenheit - 32) / 9) * 5 ;
			
			// Sa�da de informa��es
			
			System.out.println("O valor em graus Celsius � = " + celsius);
			
		}

}
