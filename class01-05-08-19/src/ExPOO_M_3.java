import java.util.Scanner;

public class ExPOO_M_3 {
	// declaration of variable globals and statics
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		// declaration of variables locals and constants
		int [][] A = new int [3][3];
		int numeroValidador = 0;
		int soma = 0;
		boolean dado;
		
		//data input
		System.out.println("Digite um n�mero para saber se existe na matriz: ");
		numeroValidador = scan.nextInt();
		
		// data processing
		System.out.println("\n---------------------------------------------");
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[i].length; j++) {
				soma += 1;
				A[i][j] = soma;
				System.out.print(A[i][j] + "	 ");
			}
			System.out.println("  ");
		}
		
		// data output	
		dado = false;
		System.out.println("---------------------------------------------");
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[i].length; j++) {
				if (A[i][j] == numeroValidador) {
					System.out.println("O n�mero digitado existe na matriz,");
					System.out.println("e est� na linha " + (i+1) + " e na coluna " + (j+1) + ".");
					dado = true;
					break;
				} else {
					//dado = false;					
				}
			}
		}
		
		if (dado == false) {
			//System.out.println("\n---------------------------------------------");
			System.out.println("\nO n�mero digitado n�o consta na lista");
		}
	}

}
