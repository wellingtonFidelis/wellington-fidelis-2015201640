import java.util.Scanner;

public class ExPOO_S_1 {
	
	// declaração de variáveis globais e static
	static Scanner tecla = new Scanner(System.in);
	
	public static void main (String[] args) {
		
		// declarando variáveis locais ou contantes
		String regiao[] = new String [10];
		int entrada = 0;
		
		// entrada de dados
		System.out.println("Programa para entrada de código e sáida da região");
		System.out.println("                                                     ");
		System.out.println("Digite o Código da Região: ");
		entrada = tecla.nextInt();
		
		// processamento de dados		
		regiao[0] = "Sul";
		regiao[1] = "Norte";
		regiao[2] = "Leste";
		regiao[3] = "Oeste";
		regiao[4] = "Nordeste";
		regiao[5] = "Sudeste";
		regiao[6] = "Centro-Oeste";
		regiao[7] = "Noroeste";
		
		// saída de dados		
		switch(entrada) {
			case 1:	System.out.println(regiao[0]);break;
			case 2: System.out.println(regiao[1]);break;
			case 3: System.out.println(regiao[2]);break;
			case 4: System.out.println(regiao[3]);break;
			case 5: System.out.println(regiao[4]);break;
			case 6: System.out.println(regiao[4]);break;
			case 7: System.out.println(regiao[5]);break;
			case 8: System.out.println(regiao[5]);break;
			case 9: System.out.println(regiao[5]);break;
			case 10: System.out.println(regiao[6]);break;
			case 11: System.out.println(regiao[7]);break;
			default: System.out.println("Importado");break;
		}
		
		
	}
	
	
}
