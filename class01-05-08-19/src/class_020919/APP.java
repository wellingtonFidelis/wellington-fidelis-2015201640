package class_020919;

import java.util.Scanner;

public class APP {
    
    //Constante
    static final int MAXCONTA = 20;
   
    //variável comum
    static int index = 0;
    
    //Lista de contas
    static Conta[] lista = new Conta[MAXCONTA];
    
    static Scanner tecla = new Scanner(System.in);

    public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir conta Corrente");
            System.out.println("2-Incluir conta Poupan�a");
            System.out.println("3-Sacar");
            System.out.println("4-Depositar");
            System.out.println("5-Listar saldo das Contas");
            System.out.println("6-Excluir conta.");
            System.out.println("7-Sair");
            System.out.println("Digite sua opção: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: incluirContaCorrente(); break;
                case 2: incluirContaPoupanca(); break;
                case 3: sacarValor(); break;
                case 4: depositarValor(); break;    
                case 5: listarContas(); break;
                case 6: excluirConta(); break;
                case 7: break;
            }
        } while (op!=7);       
    }
    
    
    public static void incluirContaCorrente(){
        //Entrada
        System.out.println("Digite o número da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o saldo da conta:");
        double saldo = tecla.nextDouble();
        System.out.println("Digite o valor do Limite:");
        double limite = tecla.nextDouble();
        //Criar o objeto e inserir na lista
        lista[index++] = new Corrente(num, saldo, limite);
        System.out.println("Conta cadastrada com sucesso!");
    }
    
    public static void incluirContaPoupanca(){
        //Entrada
        System.out.println("Digite o número da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o saldo da conta:");
        double saldo = tecla.nextDouble();
        System.out.println("Digite o valor do Limite:");
        @SuppressWarnings("unused")
		double limite = tecla.nextDouble();
        //Criar o objeto e inserir na lista
        lista[index++] = new Poupanca(num, saldo);
        System.out.println("Conta Poupan�a cadastrada com sucesso!");
    }
    
    
    public static void sacarValor(){
        //Entrada
        System.out.println("Digite o número da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do saque:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i].sacar(valor);
                break;
            }
        }
    }
    
    public static void depositarValor(){
        //Entrada
        System.out.println("Digite o número da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do depósito:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i].depositar(valor);
                break;
            }
        }
    }
    
    public static void listarContas(){
        double total = 0;
        System.out.println("Nº Conta:........ SALDO:");
        for (int i = 0; i < lista.length-1; i++) {
            if (lista[i] != null){
            	
            	Corrente c = (Corrente)lista[i];//alias, aponta para um endere�o de memoria temporaria
            	
            	
            	if ( c instanceof Corrente)
                System.out.println(c.getNumero()
                                   + "........" +
                                   c.getSaldo()
                                   + "........" +
                                   c.getLimite());
                //total += lista[i].getSaldo();
                total = total + c.getSaldo();                
            }
            
            Poupanca p = (Poupanca)lista[i];
            
            if (p instanceof Poupanca){
            	System.out.println(p.getNumero()
                        + "........" +
                        p.getSaldo()
                        + "........" +
                        p.getRendimento());
		     //total += lista[i].getSaldo();
		     total = total + p.getSaldo();
            }
        }
        System.out.println("Total:........" + total);
    }
    
    public static void excluirConta() {
    	 //Entrada
        System.out.println("Digite o número da conta:");
        int num = tecla.nextInt();
        
        //Procurar a conta na lista
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i] = null;
                break;
            }
        }
    }
    
}
