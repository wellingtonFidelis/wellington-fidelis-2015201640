package class_020919;

public class Poupanca extends Conta{

	private double rendimento;
	
	//cria��o do construtor da classe poupan�a
	public Poupanca(int numero, double saldo) {
		super(numero, saldo);
		// 
		this.rendimento = 0;
}
	//Getters and Setters
	public double getRendimento() {
		return rendimento;
	}

	public void setRendimento(double rendimento) {
		this.rendimento = rendimento;
	}

@Override
public void depositar (double valor){
		setSaldo(getSaldo() + valor);
		this.rendimento += getSaldo();
	}
}