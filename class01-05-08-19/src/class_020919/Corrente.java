package class_020919;


public class Corrente extends Conta { //herda da classe Conta
	
	private double limite;// atributo novo para a classe corrente
	
	//constructor
	public Corrente(int numero, double saldo, double limite) {
		super(numero, saldo);// invoca o construtor da classe base		
		this.limite = 0;
	}
	
	// override of methods
	
	@Override
	public void sacar (double valor) {
		if (valor < this.getSaldo()) {
			this.setSaldo(this.getSaldo() - valor * Conta.TAXA);	
		} else if (valor <= limite) {//verificar a ope��o
			this.limite -= valor;
		}
	}
	
	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}

	@Override
	public void depositar (double valor) {
		this.setSaldo(this.getSaldo() + valor);
	}

}
//api agendamento de consulta
// heroku