import java.util.Scanner;

public class ExPOO_V_1 {

	//declara��o de vari�veis globais e statics
	static Scanner teclado = new Scanner(System.in);
	
	public static void main (String[] args) {
		//declara��o de vari�veis locais e consntantes
		int x [] = new int [10];
		
		// entrada de dados
		System.out.println("Programa para ler um vetor e imprimi-lo.");
		System.out.println("-------------------------------------------------------------------");
		System.out.println("O valor de cada �ndice ser� igual a 30.");
		
		// processamento de dados
		for (int l = 0; l < x.length ; l++) {
			x[l] = 30;
		}
		
		// sa�da de dados
		for (int l = 0; l < x.length ; l++) {
			System.out.println("O valor do �ndice " + l + " � = " + x[l]);
		}
	}
}
