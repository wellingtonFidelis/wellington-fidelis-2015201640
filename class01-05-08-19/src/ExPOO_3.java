import java.util.Scanner;

public class ExPOO_3 {
	
			// Declaração de variáveis global e static
			static Scanner tecla = new Scanner (System.in);
			
			public static void main(String[] args) {
				
				// Declaração de variáveis locais e constantes
				
				double celsius, fahrenheit;
				
				// Entrada de dados
				
				System.out.println("Programa para calcular a converção de C para F.");
				
				System.out.println("Digite o valor em Celsius: ");
				celsius = tecla.nextDouble();
				
				// Processamento de dados
				
				fahrenheit = ((9 * celsius)/5) + 32 ;
				
				// Saída de informações
				
				System.out.println("O valor em graus Fahrenheit é = " + fahrenheit);
				
			}

}
