package questao02_RevisaoAV1;

import java.util.Scanner;

public class Program02 {
	// declaration of variables globals and statics
	static Scanner scan = new Scanner(System.in);
	static final int MaxConta = 10;
	static int index = 0;
	
	// list of accounts
	static Conta[] lista = new Conta[MaxConta];

	public static void main(String[] args) {
		// declaration of variables locals and constants
		int opcao = 0;
		
		do {
			System.out.println("Digite o n�mero da a��o: ");
			System.out.println("1 - Criar conta corrente.");
			System.out.println("2 - Excluir conta.");
			System.out.println("3 - Depositar.");
			System.out.println("4 - Sacar.");
			System.out.println("5 - Listar contas.");
			System.out.println("6 - Fazer transfer�ncia.");
			System.out.println("7 - Sair.");
			opcao = scan.nextInt();
			
			switch (opcao){
				case 1: criarContaCorrente(); break;
	            case 2: excluirConta(); break;
	            case 3: depositarValor(); break;
	            case 4: sacarValor(); break;    
	            case 5: listarContas(); break;
	            //case 6: excluirConta(); break;
	            case 7: break;
			}
			
		} while (opcao != 7);
	}
	
//-------------------------------------------------------------------------------------------------------------------
	
	// criar conta corrente
	public static void criarContaCorrente () {
		System.out.println("");
		System.out.println("O numero da conta ser�: " + (1000 + index));
		int numeroConta = (1000 + index);
		System.out.println("Digite o valor do saldo: ");
		double saldo = scan.nextInt();
		System.out.println("Digite o valor do limite: ");
		double limite = scan.nextInt();
		
		// cria um obejto
		lista[index++] = new ContaCorrente(numeroConta, saldo, limite);
		System.out.println("");
		System.out.println("Conta " + numeroConta + " cadastrada com sucesso!");
	}
	
	// excluir conta
	public static void excluirConta () {
		System.out.println("");
		System.out.println("Digite o n�mero da conta: ");
		int numeroConta = scan.nextInt();
		
		for (int i=0; i < lista.length; i++) {
			if (lista[i].getNumero() == numeroConta) {
				lista[i] = null; break;
			}
		}
	}
	
	//depositar algum valor
	public static void depositarValor () {
		System.out.println("");
		System.out.println("Digite o n�mero da conta: ");
		int numeroConta = scan.nextInt();
		System.out.println("Digite o valor a ser depositado: ");
		int valor = scan.nextInt();
		
		for (int i=0; i < lista.length; i++) {
			if (lista[i].getNumero() == numeroConta) {
				lista[i].depositar(valor);
			}
		}
	}
	
	//sacar algum valor
	public static void sacarValor () {
		System.out.println("");
		System.out.println("Digite o n�mero da conta: ");
		int numeroConta = scan.nextInt();
		System.out.println("Digite o valor a ser retirado: ");
		int valor = scan.nextInt();
		
		for (int i=0; i < lista.length; i++) {
			if (lista[i].getNumero() == numeroConta) {
				lista[i].sacar(valor);
			}
		}
	}
	
	//lista todas as contas e valores
	public static void listarContas () {
		System.out.println("N�mero da Conta __________ Saldo __________ Limite");
		for (int i=0; i < lista.length; i++) {
			if (lista[i] != null) {
				
				ContaCorrente c = (ContaCorrente)lista[i];
				if (c instanceof ContaCorrente) {
					System.out.println(c.getNumero() + " __________ " + c.getSaldo() + " __________ " + c.getLimite());
				}
				//
			}
		}
	}

}
