package questao02_RevisaoAV1;

public class ContaCorrente extends Conta {
	
	//constructor
	public ContaCorrente(int numero, double saldo, double limite) {
		super(numero, saldo);
		this.limite = 0;
	}

	//atributos da classe conta corrente.
	private double limite;
	
	//access methods
	public double getLimite() {
		return limite;
	}
	public void setLimite(double limite) {
		this.limite = limite;
	}
	

	@Override
	public void depositar (double valor) {
		this.setSaldo(this.getSaldo() + valor);
	}
	
	@Override
	public void sacar (double valor) {
		if (valor < this.getSaldo()) {
			this.setSaldo(this.getSaldo() - valor * Conta.TAXA);	
		} else if (valor <= limite) {//verificar a ope��o
			this.limite -= valor;
		}
	}
	

}
