import java.util.Scanner;

public class ExPOO_S_4 {
	
	// declara��o de vari�veis globais e static
	static Scanner tecla = new Scanner(System.in);
	
	public static void main (String[] args) {
		
		// declara��o de vari�veis locais e contantes
			double nota1, nota2, media;
			
			// entrada de dados
			System.out.println("Programa para c�lculo de aprova��o do aluno.");
			System.out.println("                                            ");
			System.out.println("Digite a nota da primeira prova: ");
			nota1 = tecla.nextDouble();
			System.out.println("Digite a nota da segunda prova: ");
			nota2 = tecla.nextDouble();
			
			// processamento de dados
			media = (nota1 + nota2)/2;
			
			// sa�da de dados
			if (media >= 6) {
				System.out.println("                              ");
				System.out.println("Parab�ns!!! Voc� foi aprovado.");
			}else {
				System.out.println("                              ");
				System.out.println("Reprovado!!! Estude mais.");
			}
	}
}
