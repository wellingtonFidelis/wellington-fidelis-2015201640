import java.util.Scanner;

public class ExPOO_R_5 {

	// declaração de variáveis finals e statics
	static Scanner tecla = new Scanner(System.in);
	
		public static void main (String[] args) {
			
			//declaração de variáveis globais e constantes
			double nota1, nota2, result;
			
			// entrada de dados e processamento
			System.out.println("Programa para cálculo de nota semestral.");
			System.out.println("                                                     ");
			System.out.println("Digite o valor da 1 avaliação: ");
			nota1 = tecla.nextDouble();
			if (nota1 < 0 || nota1 > 10) {
				do {
					System.out.println("Nota Inválida!");
					System.out.println("Digite outro valor para 1 avaliação: ");
					nota1 = tecla.nextDouble();
				} while (nota1 < 0 || nota1 > 10);
			}
			
			System.out.println("Digite o valor da 2 avaliação: ");
			nota2 = tecla.nextDouble();
			
			if (nota2 < 0 || nota2 > 10) {
				do {
					System.out.println("Nota Inválida!");
					System.out.println("Digite outro valor para 2 avaliação: ");
					nota2 = tecla.nextDouble();
				} while (nota2 < 0 || nota2 > 10);
			}
			
			result = (nota1 + nota2) / 2;
			
			// saída de dados
			
			System.out.println("--------------------------------------------------------------");
			System.out.println("Média semestral é = " + result );
			
		}
}
